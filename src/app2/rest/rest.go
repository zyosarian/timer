package rest

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/gorilla/handlers"
	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
	muxtrace "gopkg.in/DataDog/dd-trace-go.v1/contrib/gorilla/mux"
	//muxtrace "gopkg.in/DataDog/dd-trace-go.v1/contrib/gorilla/mux"
	"log"
	"net/http"
	"os"
)

type TimeInfo struct {
	Time        string `json:"time"`
}

func ServeAPI(endpoint string) error {

	//r := muxtrace.NewRouter()

	//r := mux.NewRouter()

	// Create a traced mux router.
	r := muxtrace.NewRouter(muxtrace.WithServiceName("middleware"))

	bookmarkRouter := r.PathPrefix("/time").Subrouter()
	bookmarkRouter.Methods("GET").Path("").HandlerFunc(handler)

	server := handlers.CORS()(r)

	return http.ListenAndServe(endpoint, server)
}

func handler(w http.ResponseWriter, r *http.Request) {

	var middlewareSpan opentracing.Span
	appSpecificOperationName := "middleware_getTime"
	wireContext, err := opentracing.GlobalTracer().Extract(
		opentracing.HTTPHeaders,
		opentracing.HTTPHeadersCarrier(r.Header))
	log.Println("Propagated headers", r.Header)
	if err != nil {
		log.Println(wireContext)
		log.Println(appSpecificOperationName + err.Error())
	}

	middlewareSpan = opentracing.StartSpan(
		appSpecificOperationName,
		ext.RPCServerOption(wireContext))
	defer middlewareSpan.Finish()
	middlewareSpan.SetTag("app", "app2")
	middlewareSpan.SetTag("kind", "middleware")
	middlewareSpan.SetTag("method", "handler")
	ctx := opentracing.ContextWithSpan(context.Background(), middlewareSpan)


	w.Header().Set("Content-Type", "application/json;charset=utf8")
	if err := apiResponse(ctx, w); err != nil {
		apiError(w, 500, "getting time data", err)
		return
	}
	w.WriteHeader(200)
}

func apiResponse(ctx context. Context, w http.ResponseWriter) error {

	span, ctx := opentracing.StartSpanFromContext(ctx, "middleware_apiResponse")
	defer span.Finish()

	span.SetTag("app", "app2")
	span.SetTag("kind", "middleware")
	span.SetTag("method", "apiResponse")

	timeInfo, err := backendResponse(ctx)
	if err != nil {
		log.Println("Error fetching time from Backend " + err.Error())
		return err
	}
	timeInfo.Time = "[" + os.Getenv("SERVICE_NAME") + "]-" + timeInfo.Time
	return json.NewEncoder(w).Encode(&timeInfo)

}

func backendResponse(ctx context.Context) (*TimeInfo, error)  {
	span, ctx := opentracing.StartSpanFromContext(ctx, "middleware_backendResponse")
	defer span.Finish()

	span.SetTag("app", "app2")
	span.SetTag("kind", "middleware")
	span.SetTag("method", "backendResponse")

	httpClient := &http.Client{}
	httpReq, _ := http.NewRequest("GET", os.Getenv("BACKEND"), nil)

	opentracing.GlobalTracer().Inject(
		span.Context(),
		opentracing.HTTPHeaders,
		opentracing.HTTPHeadersCarrier(httpReq.Header))

	resp, err := httpClient.Do(httpReq)

	//time.Sleep(time.Millisecond * 600)

	//resp, err := http.Get(os.Getenv("BACKEND"))

	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var timeInfo = TimeInfo{}
	err = json.NewDecoder(resp.Body).Decode(&timeInfo)
	return &timeInfo, err
}

func apiError(w http.ResponseWriter, code int, reason string, err error) {
	w.WriteHeader(code)
	fmt.Fprintf(w, "Error occured %s %s", reason, err)
}
