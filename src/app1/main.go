package main

import (
	"fmt"
	"github.com/opentracing/opentracing-go"
	"gopkg.in/DataDog/dd-trace-go.v1/ddtrace/opentracer"
	"gopkg.in/DataDog/dd-trace-go.v1/ddtrace/tracer"
	"os"
	"timer/src/app1/rest"
)

func main()  {
	//tracer.Start(tracer.WithServiceName("app1"))
	t := opentracer.New(tracer.WithServiceName("frontend"))
	defer tracer.Stop()
	opentracing.SetGlobalTracer(t)

	tracing()

	fmt.Println("[" + os.Getenv("SERVICE_NAME") + "] : Serving timer api...")
	if err := rest.ServeAPI("0.0.0.0:8080"); err != nil {
		panic(err)
	}
}

func tracing()  {
	t := opentracing.GlobalTracer()
	span := t.StartSpan("app1-main")
	span.SetTag("kind", "client")
	span.LogKV("message", "starting the client")
	span.Finish()
}