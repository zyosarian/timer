package rest

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/gorilla/handlers"
	"github.com/opentracing/opentracing-go"
	"log"
	"net/http"
	"os"

	muxtrace "gopkg.in/DataDog/dd-trace-go.v1/contrib/gorilla/mux"
)

type TimeInfo struct {
	Time string `json:"time"`
}

func ServeAPI(endpoint string) error {

	//r := mux.NewRouter()
	//r := muxtrace.NewRouter()

	// Create a traced mux router.
	r := muxtrace.NewRouter(muxtrace.WithServiceName("frontend"))

	bookmarkRouter := r.PathPrefix("/time").Subrouter()
	bookmarkRouter.Methods("GET").Path("").HandlerFunc(handler)

	server := handlers.CORS()(r)

	return http.ListenAndServe(endpoint, server)
}

func handler(w http.ResponseWriter, r *http.Request) {
	span, ctx := opentracing.StartSpanFromContext(r.Context(), "client_getTime")
	defer span.Finish()

	span.SetTag("app", "app1")
	span.SetTag("kind", "client")
	span.SetTag("method", "handler")
	span.LogKV("message", "getting time from middleware")


	w.Header().Set("Content-Type", "application/json;charset=utf8")
	if err := apiResponse(ctx, w); err != nil {
		apiError(w, 500, "getting time data", err)
		return
	}
	w.WriteHeader(200)
}

func apiResponse(ctx context.Context, w http.ResponseWriter) error {

	span, ctx := opentracing.StartSpanFromContext(ctx, "client_apiResponse")
	span.SetTag("app", "app1")
	span.SetTag("kind", "client")
	span.SetTag("method", "apiResponse")
	defer span.Finish()

	timeInfo, err := backendResponse(ctx)
	if err != nil {
		log.Println("Error fetching time from Backend " + err.Error())
		return err
	}
	timeInfo.Time = "[" + os.Getenv("SERVICE_NAME") + "]-" + timeInfo.Time
	return json.NewEncoder(w).Encode(&timeInfo)

}

func backendResponse(ctx context.Context) (*TimeInfo, error) {

	span, ctx := opentracing.StartSpanFromContext(ctx, "client_backendResponse")
	defer span.Finish()

	span.SetTag("app", "app1")
	span.SetTag("kind", "client")
	span.SetTag("method", "backendResponse")

	httpClient := &http.Client{}
	httpReq, _ := http.NewRequest("GET", os.Getenv("BACKEND"), nil)

	opentracing.GlobalTracer().Inject(
		span.Context(),
		opentracing.HTTPHeaders,
		opentracing.HTTPHeadersCarrier(httpReq.Header))

	resp, err := httpClient.Do(httpReq)

	//time.Sleep(time.Millisecond * 300)
	//resp, err := http.Get(os.Getenv("BACKEND"))

	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var timeInfo = TimeInfo{}
	err = json.NewDecoder(resp.Body).Decode(&timeInfo)
	return &timeInfo, err
}

func apiError(w http.ResponseWriter, code int, reason string, err error) {
	w.WriteHeader(code)
	fmt.Fprintf(w, "Error occured %s %s", reason, err)
}
