package rest

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/gorilla/handlers"
	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
	muxtrace "gopkg.in/DataDog/dd-trace-go.v1/contrib/gorilla/mux"
	"log"
	"net/http"
	"os"
	"time"
)

type TimeInfo struct {
	Time        string `json:"time"`
}

type TimeJsonTestCom struct {
	Date          string `json:"date"`
	Milliseconds  uint64 `json:"milliseconds_since_epoch"`
	Time          string `json:"time"`
}

func ServeAPI(endpoint string) error {
	//r := muxtrace.NewRouter()

	// Create a traced mux router.
	r := muxtrace.NewRouter(muxtrace.WithServiceName("backend"))

	//r := mux.NewRouter()

	bookmarkRouter := r.PathPrefix("/time").Subrouter()
	bookmarkRouter.Methods("GET").Path("").HandlerFunc(handler)

	server := handlers.CORS()(r)

	return http.ListenAndServe(endpoint, server)
}

func handler(w http.ResponseWriter, r *http.Request) {

	var backendSpan opentracing.Span
	appSpecificOperationName := "backend_getTime"
	wireContext, err := opentracing.GlobalTracer().Extract(
		opentracing.HTTPHeaders,
		opentracing.HTTPHeadersCarrier(r.Header))
	if err != nil {
		log.Println(appSpecificOperationName + err.Error())
	}

	backendSpan = opentracing.StartSpan(
		appSpecificOperationName,
		ext.RPCServerOption(wireContext))
	defer backendSpan.Finish()
	backendSpan.SetTag("app", "app3")
	backendSpan.SetTag("kind", "backend")
	backendSpan.SetTag("method", "handler")

	ctx := opentracing.ContextWithSpan(context.Background(), backendSpan)

	w.Header().Set("Content-Type", "application/json;charset=utf8")
	if err := apiResponse(ctx, w); err != nil {
		apiError(w, 500, "getting time data", err)
		return
	}
	w.WriteHeader(200)
}

func apiResponse(ctx context.Context, w http.ResponseWriter) error {
	span, ctx := opentracing.StartSpanFromContext(ctx, "backend_apiResponse")
	span.SetTag("app", "app3")
	span.SetTag("kind", "backend")
	span.SetTag("method", "apiResponse")
	defer span.Finish()

	timeInfo, _ := backendResponse(ctx)
	storeTimeInfoToRedis(ctx)
	return json.NewEncoder(w).Encode(timeInfo)
}

func storeTimeInfoToRedis(ctx context.Context)  {
	span, ctx := opentracing.StartSpanFromContext(ctx, "backend_storeTimeInfoToRedis")
	defer span.Finish()

	span.SetTag("app", "app3")
	span.SetTag("kind", "backend")
	span.SetTag("method", "storeTimeInfoToRedis")

	time.Sleep(time.Millisecond * 150)
}

func backendResponse(ctx context.Context) (*TimeInfo, error)  {
	span, ctx := opentracing.StartSpanFromContext(ctx, "backend_backendResponse")
	defer span.Finish()

	span.SetTag("app", "app3")
	span.SetTag("kind", "backend")
	span.SetTag("method", "backendResponse")

	httpClient := &http.Client{}
	httpReq, _ := http.NewRequest("GET", "http://time.jsontest.com", nil)

	opentracing.GlobalTracer().Inject(
		span.Context(),
		opentracing.HTTPHeaders,
		opentracing.HTTPHeadersCarrier(httpReq.Header))

	resp, err := httpClient.Do(httpReq)

//	timeInfo := TimeInfo{Time: "[" + os.Getenv("SERVICE_NAME") + "] " + currentTime.Format("2006-01-02 15:04:05.000000")}


	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	//time.Sleep(time.Millisecond * 4000)

	var externalTime = TimeJsonTestCom{}

	err = json.NewDecoder(resp.Body).Decode(&externalTime)
	defer resp.Body.Close()

	if err != nil {
		log.Println("error decoding time" + err.Error())
		return nil, err
	}

	timeInfo := TimeInfo{Time: "[" + os.Getenv("SERVICE_NAME") + "] " + externalTime.Time}

	log.Println("current time", externalTime)

	return &timeInfo, err
}

func apiError(w http.ResponseWriter, code int, reason string, err error) {
	w.WriteHeader(code)
	fmt.Fprintf(w, "Error occured %s %s", reason, err)
}
