package main

import (
	"fmt"
	"github.com/opentracing/opentracing-go"
	"gopkg.in/DataDog/dd-trace-go.v1/ddtrace/opentracer"
	"gopkg.in/DataDog/dd-trace-go.v1/ddtrace/tracer"
	"os"
	"timer/src/app3/rest"
)

func main()  {
	t := opentracer.New(tracer.WithServiceName("backend"))
	defer tracer.Stop()
	opentracing.SetGlobalTracer(t)

	fmt.Println("[" + os.Getenv("SERVICE_NAME") + "] : Serving timer api...")
	if err := rest.ServeAPI("0.0.0.0:8080"); err != nil {
		panic(err)
	}
}
