default:
	docker build -f Dockerfiles/app1.Dockerfile -t app1 .
	docker build -f Dockerfiles/app2.Dockerfile -t app2 .
	docker build -f Dockerfiles/app3.Dockerfile -t app3 .

up: default
	docker-compose up

logs:
	docker-compose logs -f

down:
	docker-compose down && docker rm app1 app2 app3
