module timer

go 1.15

require (
	github.com/DataDog/datadog-go v4.1.0+incompatible // indirect
	github.com/google/uuid v1.1.2 // indirect
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	github.com/opentracing-contrib/go-stdlib v1.0.0
	github.com/opentracing/opentracing-go v1.2.0
	github.com/philhofer/fwd v1.1.0 // indirect
	golang.org/x/time v0.0.0-20200630173020-3af7569d3a1e // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	gopkg.in/DataDog/dd-trace-go.v1 v1.27.1
)
