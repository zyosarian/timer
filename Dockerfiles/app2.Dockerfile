FROM golang:1.13 as build

RUN apt update && apt install dnsutils -y

ENV GO111MODULE=on

WORKDIR /timer/services/app2

COPY go.mod .

COPY go.sum .

RUN go mod download

RUN ["go", "get", "github.com/githubnemo/CompileDaemon"]

ENTRYPOINT CompileDaemon -exclude-dir=.git -exclude-dir=.idea -log-prefix=false -build="go build -o ./bin/app2 ./src/app2" -command="./bin/app2"
